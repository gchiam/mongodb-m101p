from pymongo import MongoClient
from operator import itemgetter

db = MongoClient().school

for each in db.students.find().sort([("student_id", 1)]):
    scores = each["scores"]
    lowest = None
    sorted_homeworks = sorted(
        filter(lambda x: x['type'] == 'homework', scores),
        key=itemgetter('score')
    )
    lowest_homework = sorted_homeworks[0]
    scores.remove(lowest_homework)

    print "Removing: %s" % lowest_homework
    db.students.update(
        {"_id": each["_id"]},
        {"$pull": {"scores": lowest_homework}}
    )
