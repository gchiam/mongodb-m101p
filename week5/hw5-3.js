var results = db.grades.aggregate(
    [
        {
            $unwind: "$scores"
        },
        {
            $match: {
                $or: [ { "scores.type": "exam" }, { "scores.type": "homework" } ]
            }
        },
        {
            $group: {
                _id: { class_id: "$class_id", student_id: "$student_id" },
                avg_score: {$avg: "$scores.score" }
            }
        },
        {
            $group: {
                _id: { class_id: "$_id.class_id" },
                avg_score: {$avg: "$avg_score" }
            }
        },
        {
            $project: {
                _id: 0,
                class_id: "$_id.class_id",
                avg_score: 1
            }
        },
        {
            $sort: 
                { avg_score: -1 }
        },
        {
            $limit: 1
        }
    ]
)
printjson(results)
