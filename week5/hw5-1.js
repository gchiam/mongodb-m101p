var results = db.posts.aggregate([
    { $project: {
        _id: 0,
        comments: 1
    }},
    { $unwind: "$comments" },
    { $project: {
        author: "$comments.author"
    }},
    { $group : {
        _id : "$author",
        commentsPerAuthor : { $sum : 1 },
    }},
    { $sort : { 
        commentsPerAuthor: -1 
    }},
    { $limit : 1 }
]);

printjson (results);
