var result = db.zips.aggregate(
    [
        {
            $match: {
                $or: [ {state: "CA"}, {state: "NY"} ],
            }
        },
        {
            $project: {
                _id: 0,
                pop: 1,
                city: 1,
                state: 1
            }
        },
        {
            $group: {
                _id: { state: "$state", city: "$city" },
                pop: { $sum: "$pop" }
            }
        },
        {
            $match: {
                pop: { $gt: 25000 }
            }
        },
        {
            $group: {
                _id: null,
                average_pop: { $avg: "$pop" }
            }
        },
        {
            $project: {
                _id: 0,
                average_pop: 1
            }
        }
    ]
)
printjson (result.result)
