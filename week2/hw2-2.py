from pymongo import MongoClient
db = MongoClient().students

prev_student_id = None
for each in db.grades.find({"type": "homework"}).sort([("student_id", 1), ("score", 1)]):
    if each["student_id"] != prev_student_id:
        prev_student_id = each["student_id"]
        print "Removing: %s" % each
        db.grades.remove(each)
