var results = db.messages.aggregate([
    //{ $match: { "headers.From": "susan.mara@enron.com" }},
    { $project: { from: "$headers.From", to: "$headers.To" }},
    { $unwind: "$to" },
    { $group: { _id: "$_id", from: { $first: "$from" }, to: { $addToSet: "$to" }}},
    { $project: { _id: 0, from: 1, to: 1 } },
    { $unwind: "$to" },
    { $group: { _id: { from: "$from", to: "$to"}, count: { $sum: 1 }}},
    { $sort: { count: -1 }},
    { $limit: 1 },
    { $project: { _id: 0, from: "$_id.from", to: "$_id.to", count:1 }}
]);
printjson (results);

// verify
var from = results.result[0].from;
var to = results.result[0].to;
print (db.messages.find({"headers.From": from, "headers.To": to}).count());

